import cv2 as cv
import numpy as np
import imutils
from draw import *
import os
from g_streamer import g_streamer_pipeline
from qr_scanner import scanner
from get_predictions import return_predictions

detector = cv.QRCodeDetector()

# Parameters
thickness_text = 1
thickness = -1
fontScale = 0.8
fontScale_email = 2
fontScale_prediction = 0.6
font = cv.FONT_HERSHEY_SIMPLEX
thickness_text = 1
fontScale_3 = 0.3
line_thickness = 2
light_green_color = (0,255,100)
black_color = (0,0,0)
white_color = (255,255,255)

# Coordinates
text_org = (350, 25)
text_org_qr = (300, 50)
trying_to_predict_rect,  trying_to_predict_rect2= (560, 5), (350, 35)
text_predictions_left = (20, 90)
text_predictions_right = (500, 90)

# Roi for prediction rectangles
left, top, right, bottom = 20, 100, 400, 450
left2, top2, right2, bottom2 = 500, 100, 880, 450

def show_camera(video_capture):
    stop_scan = False
    delay = 1
    changing_text = ""
    
    print(g_streamer_pipeline(flip_method=0))
    if video_capture.isOpened():
            while True:
                
                # If no QR code scanned
                if(not stop_scan):
                    changing_text = ""

                ret, frame = video_capture.read()
                
                # Qr scanner
                qr_frame = frame.copy()
                data, bbox, _ = detector.detectAndDecode(qr_frame)
                stop_scan = scanner(data)
                
                ret = cv.waitKey(delay) & 0xFF

                if ret == ord('c'): 
                    print("Continue")
                    stop_scan = False
                    delay = 1
                
                # Send e-mail if QR code detected
                if(stop_scan): 
                    changing_text = "Email sent!"
                    print("Stop scan")
                    stop_scan = True
                    delay = 500
                    continue
                    

                # Flip to mirror movements in camera correctly
                frame = cv.flip(frame, 1)
                
                # Return predictions from model
                text_left, text_right, prediction = return_predictions(frame)
                
                # Labels
                cat_list = ['raspberry', 'ultrasonic', 'led','button']
                
                # Rectangles to make predictions
                cv.rectangle(frame, (left, top), (right, bottom), light_green_color, line_thickness)
                cv.rectangle(frame, (left2, top2), (right2, bottom2), light_green_color, line_thickness)
                
                if(prediction):
                    # Left
                    frame = cv.putText(frame, text_left, text_predictions_left,font, fontScale_prediction, light_green_color, thickness_text)
                    
                    # Right
                    frame = cv.putText(frame, text_right, text_predictions_right,font, fontScale_prediction, light_green_color, thickness_text)
                
                # Frame for e-mail sent text
                frame = cv.putText(frame, changing_text, text_org_qr,font, fontScale_email, light_green_color, line_thickness)
                # Convert and return frame
                ret, buffer = cv.imencode('.jpg', frame)
                frame = buffer.tobytes()
                yield (b'--frame\r\n'
                       b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')