def g_streamer_pipeline(
    sensor_id=0,
    capture_width=900,
    capture_height=500,
    display_width=900,
    display_height=500,
    framerate=1,
    flip_method=0,
):
    return (
        "nvarguscamerasrc sensor_id=%d !"
        "video/x-raw(memory:NVMM), width=(int)%d, height=(int)%d, framerate=(fraction)%d/1 ! "
        "nvvidconv flip-method=%d ! "
        "video/x-raw, width=(int)%d, height=(int)%d, format=(string)BGRx ! "
        "videoconvert ! "
        "video/x-raw, format=(string)BGR ! appsink"
        % (
            sensor_id,
            capture_width,
            capture_height,
            framerate,
            flip_method,
            display_width,
            display_height,
        )
    )