from email_image import Emailer

def scanner(data):
    # Object of class Emailer
    sender = Emailer()
    stop_scan = False
    
    # If a QR-code is scanned
    if(data):
        qr_code=data
        
        # Split name from e-mail
        split_qr = data.split(" ")
        name = split_qr[0]
        email = split_qr[1]
        sendTo = email
        # Get image to send
        image = 'image.jpg'
        emailSubject = "Raspberry Connector"
        emailContent = "Hello " + name + "," +  "\n"  + "Here is the image from Raspberry Connector." +  "\n"  + "Have a nice day!" 
        sender.sendmail(sendTo, emailSubject, emailContent, image)
        print("Email Sent")
        
        # Set stop QR scanner to true
        stop_scan = True
    
    return stop_scan