from flask import Flask, render_template, Response
import cv2 as cv
import os
from g_streamer import g_streamer_pipeline
from display_image import show_camera

video_capture = cv.VideoCapture(g_streamer_pipeline(flip_method=0), cv.CAP_GSTREAMER)

# Remove old image
try:
    os.remove('image.jpg')
except:
    print("No image file")

# Flask app
app = Flask(__name__)

def image():
    while True:
        new_img = cv.imread("image.jpg", cv.IMREAD_COLOR)
        if new_img is not None:
            ret, buffer = cv.imencode('.jpg', new_img)
            new_img = buffer.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + new_img + b'\r\n')
    
@app.route('/')
def index():
    return render_template('index.html')

@app.route('/video_feed')
def video_feed():
    return Response(show_camera(video_capture), 
                    mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/show_image')
def show_image():
    return Response(image(), 
                    mimetype='multipart/x-mixed-replace; boundary=frame')

app.run(host="0.0.0.0", port=80)
video_capture.release()
