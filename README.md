# Raspberry Connector

![](rpi_ultra.PNG)

## Description
This project is an assignment from [Noroff](https://www.noroff.no/en/), as part of [Experis Academy](https://www.experis.se/sv/it-tjanster/experis-academy). It is also a part of Nvidia's [Getting Started with AI on Jetson Nano](https://courses.nvidia.com/courses/course-v1:DLI+S-RX-02+V2/) and [Jetson AI Specialist certification](https://developer.nvidia.com/embedded/learn/jetson-ai-certification-programs).
<br />
A short video demo of the project can be found [here](https://youtu.be/V7Oqwy9junc).

## Table of Contents
- [Background](#background)
- [Install](#install)
- [Assignment](#assignment)
- [Implementation](#implementation)
- [Install](#install)
- [Usage](#usage)
- [Contributor](#contributor)
- [References](#references)
- [License](#license)

## Background
This project is a computer vision project based on Jetson nano. It uses a CNN model to classify a raspberry pi and 3 different devices:
1. Ultrasonic sensor
2. Button
3. Led

![](devices.jpg)

It is only possible to classify 1 raspberry pi together with 1 of the devices.
<br/>
When a raspberry pi is classified together with one of the devices, an image will show up on the screen with a suggestion how to connect the 2 parts. The image can then be sent to an email by scanning a QR-code in the camera. 
![](display_image.png)

### Assignment
This project started with the course [Getting Started with AI on Jetson Nano](https://courses.nvidia.com/courses/course-v1:DLI+S-RX-02+V2/) from [Nvidia](https://www.nvidia.com/en-us/training/). It is a project for Nvidia's [Jetson AI Specialist certification](https://developer.nvidia.com/embedded/learn/jetson-ai-certification-programs) as well.
<br/>


## Implementation

### CNN model
There are 2 models added. 1 of the model is trained with the base from the course [Getting Started with AI on Jetson Nano](https://courses.nvidia.com/courses/course-v1:DLI+S-RX-02+V2/). The other model can be be found in the folder ```model/pytorch_cnn.ipynb```. Both models are based on PyTorch. The model from the course by Nvidia is pretrained with [Resnet18](https://pytorch.org/vision/main/models/generated/torchvision.models.resnet18.html).
- Model from Nvidia: ```model2.pth```
- Other model: ```cnn_model.pth```

### OpenCv
The videostreaming is based on OpenCv. All images with the suggestions how to connect the devices are also made in OpenCv and will be saved as 1 image (```image.jpg```). 

### Send e-mail
[smtplib](https://docs.python.org/3/library/smtplib.html) is used to send an email from a Gmail account to a choosen email address. When adding a qr-code it should be added as "yourname email". You can create a qr-code [here](https://qr-code-generator.org/#text).
![](email.PNG)
![](sent_email.jpg)

## Install

**Hardware**
- [Jetson nano (4GB)](https://www.nvidia.com/en-us/autonomous-machines/embedded-systems/jetson-nano/)
- USB or CSI camera for the Jetson nano
- SD-card (64GB)

**Software**
- [Python 3.6](https://www.python.org/)
- [OpenCv](https://opencv.org/)
- [PyTorch](https://pytorch.org/)
- [Flask](https://pypi.org/project/Flask/)
- [Matplotlib](https://matplotlib.org/)
- [smtplib](https://docs.python.org/3/library/smtplib.html)


## Usage

### Create a model
Go to the folder:

```
model/pytorch_cnn.ipynb
```
Add your own dataset if you want, or use the attached dataset with train and test images. After this you can run the mode. You can also use the trained model from the course [Getting Started with AI on Jetson Nano](https://courses.nvidia.com/courses/course-v1:DLI+S-RX-02+V2/). Join the course to find out more.

### Run the project
Clone the repo. Go to your terminal and add:
```
python3 main.py
```

### Please note!
- If you want to be able to send an email you will need to add your credentials in the python file ```email_image.py```.
- If you are using an USB camera you might have to change the settings in the file ```g_streamer.py```.

## Contributor
This is a project created by [Helena Barmer](https://gitlab.com/helenabarmer).

## References
- [Getting Started with AI on Jetson Nano](https://courses.nvidia.com/courses/course-v1:DLI+S-RX-02+V2/)
- [ JetsonHacksNano/CSI-Camera ](https://github.com/JetsonHacksNano/CSI-Camera/blob/master/simple_camera.py)
- [Sending An Email With Attached Photo Using Python And The Raspberry Pi](https://bc-robotics.com/tutorials/sending-email-attached-photo-using-python-raspberry-pi/)
- [Free QR Code Generator](https://qr-code-generator.org/#text)
- [Video Streaming with Flask](https://blog.miguelgrinberg.com/post/video-streaming-with-flask)
- [Waveshare JETANK GitHub](https://github.com/waveshare/JETANK)

## License
[MIT](https://en.wikipedia.org/wiki/MIT_License)