import cv2 as cv
import numpy as np

# Colors
white_color = (255,255,255)
black_color = (0,0,0)
blue_color = (255,0,0)
red_color =(0,0,255)
silver_color = (192,192,192)
gray_color = (128,128,128)
green_color = (0, 128, 0) 
yellow_color = (0,255,255)
light_green_color = (0,255,100)
brown_color = (0,80,100)
light_blue_color = (100,150,0)

# Parameters
thickness_text = 1
thickness = -1
fontScale = 0.4
font = cv.FONT_HERSHEY_SIMPLEX
thickness_text = 1
fontScale_3 = 0.3
line_thickness = 2

def draw_raspberry(img):
    # Base rpi coordinates
    start = (20, 20)
    end = (120, 150)
        
    # Charger coordinates
    charger_start = (25,130)
    charger_end = (115,150)
        
    # Text USB/Chargers coordinates
    usb_org = (30,145)
    
    # RPI text coordinates
    rpi_org = (20, 15)
        
    # Parameters
    radius = 1
    
    # Pins coordinates 2-40 / right side
    center_coordinates_pin_2 = (115,25)
    center_coordinates_pin_4 = (115,30)
    center_coordinates_pin_6 = (115,35)
    center_coordinates_pin_8 = (115,40)
    center_coordinates_pin_10 = (115,45)
    center_coordinates_pin_12 = (115,50)
    center_coordinates_pin_14 = (115,55)
    center_coordinates_pin_16 = (115,60)
    center_coordinates_pin_18 = (115,65)
    center_coordinates_pin_20 = (115,70)
    center_coordinates_pin_22 = (115,75)
    center_coordinates_pin_24 = (115,80)
    center_coordinates_pin_26 = (115,85)
    center_coordinates_pin_28 = (115,90)
    center_coordinates_pin_30 = (115,95)
    center_coordinates_pin_32 = (115,100)
    center_coordinates_pin_34 = (115,105)
    center_coordinates_pin_36 = (115,110)
    center_coordinates_pin_38 = (115,115)
    center_coordinates_pin_40 = (115,120)
    
    # Pins coordinates 1-39 / left side
    center_coordinates_pin_1 = (110,25)
    center_coordinates_pin_3 = (110,30)
    center_coordinates_pin_5 = (110,35)
    center_coordinates_pin_7 = (110,40)
    center_coordinates_pin_9 = (110,45)
    center_coordinates_pin_11 = (110,50)
    center_coordinates_pin_13 = (110,55)
    center_coordinates_pin_15 = (110,60)
    center_coordinates_pin_17 = (110,65)
    center_coordinates_pin_19 = (110,70)
    center_coordinates_pin_21 = (110,75)
    center_coordinates_pin_23 = (110,80)
    center_coordinates_pin_25 = (110,85)
    center_coordinates_pin_27 = (110,90)
    center_coordinates_pin_29 = (110,95)
    center_coordinates_pin_31 = (110,100)
    center_coordinates_pin_33 = (110,105)
    center_coordinates_pin_35 = (110,110)
    center_coordinates_pin_37 = (110,115)
    center_coordinates_pin_39 = (110,120)
        
    # Rectangle RPI base
    img = cv.rectangle(img, start, end, green_color, thickness)
        
    # Rectangle USB and chargers
    img = cv.rectangle(img, charger_start, charger_end, silver_color, thickness)
        
    # Text USB and chargers
    img = cv.putText(img, 'USB and ethernet', usb_org, font, fontScale_3, black_color, thickness_text, cv.LINE_AA)
    
    # Raspberry pi
    img = cv.putText(img, 'Raspberry Pi', rpi_org, font, fontScale_3, black_color, thickness_text, cv.LINE_AA)
        
    # Pins 1-20 right side
    img = cv.circle(img, center_coordinates_pin_2, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_4, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_6, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_8, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_10, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_12, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_14, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_16, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_18, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_20, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_22, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_24, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_26, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_28, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_30, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_32, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_34, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_36, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_38, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_40, radius, silver_color, thickness)
    
    # Pins 1-39 / Left side
    img = cv.circle(img, center_coordinates_pin_1, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_3, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_5, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_7, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_9, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_11, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_13, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_15, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_17, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_19, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_21, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_23, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_25, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_27, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_29, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_31, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_33, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_35, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_37, radius, silver_color, thickness)
    img = cv.circle(img, center_coordinates_pin_39, radius, silver_color, thickness)
    
    return img


def ultrasonic_image(img):
    ultra_start = (210,20)
    ultra_end = (280,50)

    # Text coordinates
    ultra_org = (205,15)
    
    # Parameters
    radius = 12
          
    # Start for ultrasonic
    img = cv.rectangle(img, ultra_start, ultra_end, blue_color, thickness)
    
    # Circles left and right
    cord_circle_left = (225,35)
    cord_circle_right = (265,35)
    img = cv.circle(img, cord_circle_left, radius, silver_color, thickness)
    img = cv.circle(img, cord_circle_right, radius, silver_color, thickness)
    
    # Text
    img = cv.putText(img, 'Ultrasonic sensor', ultra_org, font, fontScale_3, black_color, thickness_text, cv.LINE_AA)
        
    # Coordinates
    # VCC
    vcc_start_point = (230, 65)
    vcc_end_point = (230, 52)
        
    # Trig
    trig_start_point = (240, 65)
    trig_end_point = (240, 52)
        
    #Echo
    echo_start_point = (250, 65)
    echo_end_point = (250, 52)
        
    # GND
    gnd_start_point = (260, 65)
    gnd_end_point = (260, 52)


    # Using cv2.line() method
    # Draw a diagonal green line with thickness of 9 px
    img = cv.line(img, vcc_start_point, vcc_end_point, red_color, line_thickness)
    img = cv.line(img, trig_start_point, trig_end_point, blue_color, line_thickness)
    img = cv.line(img, echo_start_point, echo_end_point, green_color, line_thickness)
    img = cv.line(img, gnd_start_point, gnd_end_point, black_color, line_thickness)
    
    return img

def board_image(img):
    # Base coordinates
    start = (200, 150)
    end = (295, 295)
           
    # Dot coordinates
    dot_plus = (205, 165)
    dot_minus = (220, 165)
    
    dot_plus2 = (205, 180)
    dot_minus2 = (220, 180)
    
    dot_minus_a6 = (220, 240)
    
    dot_a_3 = (242, 195)
    dot_b_3 = (250, 195)
    dot_c_3 = (258, 195)
    dot_d_3 = (266, 195)
    dot_e_3 = (274, 195)
    dot_f_3 = (290, 195)
    
    dot_a_4 = (242, 210)
    dot_b_4 = (250, 210)
    dot_c_4 = (258, 210)
    dot_d_4 = (266, 210)
    dot_e_4 = (274, 210)
    dot_f_4 = (290, 210)
    
    dot_a_5 = (242, 225)
    dot_b_5 = (250, 225)
    dot_c_5 = (258, 225)
    dot_d_5 = (266, 225)
    dot_e_5 = (274, 225)
    dot_f_5 = (290, 225)
    
    dot_a_6 = (242, 240)
    dot_b_6 = (250, 240)
    dot_c_6 = (258, 240)
    dot_d_6 = (266, 240)
    dot_e_6 = (274, 240)
    dot_f_6 = (290, 240)
        
    # Text coordinates
    text_cord = (200, 290)
    text__plus = (200, 160)
    text__minus = (215, 160)
    text__plus_bottom = (200, 280)
    text__minus_bottom = (215, 280)
    alpha = (240, 160)
    alpha_bottom = (240, 275)
        
    one = (230, 170)
    two = (230, 185)
    three = (230, 200)
    four = (230, 215)
    five = (230, 230)
    six = (230, 245)
    seven = (230, 260)
        
    # Parameters
    radius = 4
    radius_alpha = 3
        
    # Shapes
    img = cv.rectangle(img, start, end, silver_color, thickness)
    img = cv.circle(img, dot_plus, radius, gray_color, thickness)
    img = cv.circle(img, dot_minus, radius, gray_color, thickness)
    img = cv.circle(img, dot_plus2, radius, gray_color, thickness)
    img = cv.circle(img, dot_minus2, radius, gray_color, thickness)
    img = cv.circle(img, dot_minus_a6, radius, gray_color, thickness)
    img = cv.circle(img, dot_a_3, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_b_3, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_c_3, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_d_3, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_e_3, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_f_3, radius_alpha, gray_color, thickness)
    
    img = cv.circle(img, dot_a_4, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_b_4, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_c_4, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_d_4, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_e_4, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_f_4, radius_alpha, gray_color, thickness)
    
    img = cv.circle(img, dot_a_5, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_b_5, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_c_5, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_d_5, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_e_5, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_f_5, radius_alpha, gray_color, thickness)
    
    img = cv.circle(img, dot_a_6, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_b_6, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_c_6, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_d_6, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_e_6, radius_alpha, gray_color, thickness)
    img = cv.circle(img, dot_f_6, radius_alpha, gray_color, thickness)

    # Text
    img = cv.putText(img, 'Board', text_cord, font, fontScale_3, black_color, thickness_text, cv.LINE_AA)
    img = cv.putText(img, '+', text__plus, font, fontScale, red_color, thickness_text)
    img = cv.putText(img, '-', text__minus, font, fontScale, blue_color, thickness_text)
    img = cv.putText(img, '+', text__plus_bottom, font, fontScale, red_color, thickness_text)
    img = cv.putText(img, '-', text__minus_bottom, font, fontScale, blue_color, thickness_text)
    img = cv.putText(img, 'abcde  f', alpha, font, fontScale, black_color, thickness_text)
    img = cv.putText(img, 'abcde  f', alpha_bottom, font, fontScale, black_color, thickness_text)
    img = cv.putText(img, '1', one, font, fontScale, black_color, thickness_text)
    img = cv.putText(img, '2', two, font, fontScale, black_color, thickness_text)
    img = cv.putText(img, '3', three, font, fontScale, black_color, thickness_text)
    img = cv.putText(img, '4', four, font, fontScale, black_color, thickness_text)
    img = cv.putText(img, '5', five, font, fontScale, black_color, thickness_text)
    img = cv.putText(img, '6', six, font, fontScale, black_color, thickness_text)
    img = cv.putText(img, '7', seven, font, fontScale, black_color, thickness_text)
    
    return img

def draw_line(img, start, end, color):
    img = cv.line(img, start, end, color, line_thickness)
    return img

def rpi_5v_gnd(img):
    # 5V
    img = draw_line(img, (115,25), (205, 180), red_color) # Pin 2 to board
    img = cv.putText(img, 'Pin 2 (5V)', (20,170), font, fontScale, red_color, thickness_text, cv.LINE_AA)
    
    # GND
    img = draw_line(img, (115,35), (220, 170), black_color) # Pin 6 to board
    img = cv.putText(img, 'Pin 6 (GND)', (20,190), font, fontScale, black_color, thickness_text, cv.LINE_AA)
    
    return img

def ultrasonic_wires(img):

    #VCC
    img = draw_line(img, (230, 65), (205, 165), red_color) # Vcc sensor to board
    
    # GND sensor
    img = draw_line(img, (260, 65), (220, 165), black_color) # GND on sensor to board
    
    # RPI to ground 5V
    img = rpi_5v_gnd(img) # RPI to board GND and 5V

    #TRIG
    img = draw_line(img, (115,60), (240, 65), blue_color) # Pin 16 to trig
    img = cv.putText(img, 'Pin 16 (Trig)', (20,210), font, fontScale, blue_color, thickness_text, cv.LINE_AA)
    
    #ECHO
    img = draw_line(img, (250, 65), (266, 210), green_color) # Echo to board, d-4
    img = cv.putText(img, 'Echo to board d4', (20,230), font, fontScale, green_color, thickness_text, cv.LINE_AA)
    # Resistor 1K
    img = draw_line(img, (258, 210), (258, 240), brown_color) # Board: c4 to c6
    img = cv.putText(img, '1K resistor', (20,250), font, fontScale, brown_color, thickness_text, cv.LINE_AA)
    
    # Resistor 2K
    img = draw_line(img, (242, 240), (220, 240), light_blue_color) # Board: a6 to ground
    img = cv.putText(img, '2K resistor (to ground "-")', (20,270), font, fontScale, light_blue_color, thickness_text, cv.LINE_AA)
    
    # Pin 40 in between resistors
    img = draw_line(img, (250, 240), (115,120), gray_color) # Board: b6 to pin 40 (GPIO 21)
    img = cv.putText(img, 'Wire b6 to pin 40', (20,290), font, fontScale, gray_color, thickness_text, cv.LINE_AA)
    return img


def button_wires(img):
    # Leg 1 to board c3
    img = draw_line(img, (208, 45), (258, 195), brown_color) 
    
    # Leg 2 to board c5
    img = draw_line(img, (208, 25), (258, 225), green_color) 
    
    # Leg 3 to board f3
    img = draw_line(img, (250, 45), (290, 195), light_green_color) 
    
    # Leg 4 to board f5
    img = draw_line(img, (250, 25), (290, 225), light_blue_color) 
    
    # Wire to GND pin 6 to a3
    img = draw_line(img, (115,35), (242, 195), black_color) 
    
    # Wire to Pin 12 to a5 
    img = draw_line(img, (115,50), (242, 225), blue_color) 
    
    # Text
    img = cv.putText(img, 'Leg 1 to board c3', (20,170), font, fontScale, brown_color, thickness_text, cv.LINE_AA)
    img = cv.putText(img, 'Leg 2 to board c5', (20,190), font, fontScale, green_color, thickness_text, cv.LINE_AA)
    img = cv.putText(img, 'Leg 3 to board f3', (20,210), font, fontScale, light_green_color, thickness_text, cv.LINE_AA)
    img = cv.putText(img, 'Leg 4 to board f5', (20,230), font, fontScale, light_blue_color, thickness_text, cv.LINE_AA)
    img = cv.putText(img, 'Wire: GND pin 6 to a3', (20,250), font, fontScale, black_color, thickness_text, cv.LINE_AA)
    img = cv.putText(img, 'Wire: Pin 12 to a5', (20,270), font, fontScale, blue_color, thickness_text, cv.LINE_AA)
    
    return  img

def led_wires(img):
    
    # Anode (long) to board e4
    img = draw_line(img, (249, 105), (274, 210), brown_color) 
    
    # Cathode (short) to board e3
    img = draw_line(img, (252, 120), (274, 195), light_green_color) 
    
    # Resistor 100Ω anode to resistor (long) not done (d4 to d6)
    img = draw_line(img, (266, 210), (266, 240), blue_color) 
    
    # Wire GND to board cathode (short) to the ground a3 on board
    img = draw_line(img, (115,35), (242, 195), black_color) 
    
    # Wire Pin 12 to board to a6
    img = draw_line(img, (115,50), (242, 240), light_blue_color) 
    
    # Text
    img = cv.putText(img, 'Anode (long) to e4', (20,170), font, fontScale, brown_color, thickness_text, cv.LINE_AA)
    img = cv.putText(img, 'Cathode (short) to e3', (20,190), font, fontScale, light_green_color, thickness_text, cv.LINE_AA)
    img = cv.putText(img, 'Resistor 100 Ohm', (20,210), font, fontScale, blue_color, thickness_text, cv.LINE_AA)
    img = cv.putText(img, 'GND to a3', (20,230), font, fontScale, black_color, thickness_text, cv.LINE_AA)
    img = cv.putText(img, 'Pin 12 to a6', (20,250), font, fontScale, light_blue_color, thickness_text, cv.LINE_AA)
    
    return img

def save_to_image(img, device, wires):
    left, top, right, bottom = 0, 5, 350, 300
    white_background = np.full((img.shape), white_color, dtype=np.uint8)
    white_background = draw_raspberry(white_background)
    white_background = device(white_background)
    white_background = board_image(white_background)
    white_background = wires(white_background)
    white_background = white_background[top:bottom, left:right] # Crop image
    cv.imwrite('image.jpg', white_background)
    
def button_image(img):
    # Button
    button_start = (210,20)
    button_end = (240,50)
        
    button_rect_start = (212,22)
    button_rect_end = (238,48)
        
    # Side circles
    circle_1 = (215,25)
    circle_2 = (235,25)
    circle_3 = (215,45)
    circle_4 = (235,45)
        
    # Circle coordinates
    cord_circle_left = (225,35)

    # Text coordinates
    button_org = (210,15)

    # Parameters
    radius = 8

    # Start for button rectangle
    img = cv.rectangle(img, button_start, button_end, black_color, thickness)
        
    # Rectangle in the button
    img = cv.rectangle(img, button_rect_start, button_rect_end, silver_color, thickness)

    # Circles in button
    img = cv.circle(img, cord_circle_left, radius, black_color, thickness)
        
    # Circles on the sides
    img = cv.circle(img, circle_1, 2, black_color, thickness)
    img = cv.circle(img, circle_2, 2, black_color, thickness)
    img = cv.circle(img, circle_3, 2, black_color, thickness)
    img = cv.circle(img, circle_4, 2, black_color, thickness)
 
    # Text
    img = cv.putText(img, 'Button', button_org, font, fontScale_3, black_color, thickness_text, cv.LINE_AA)

    # Coordinates
    # Leg 1
    leg1_start_point = (208, 45)
    leg1_end_point = (200, 45)

    # Leg 2
    trig_start_point = (208, 25)
    trig_end_point = (200, 25)

    # Leg 3
    echo_start_point = (250, 45)
    echo_end_point = (242, 45)

    # Leg 4
    gnd_start_point = (250, 25)
    gnd_end_point = (242, 25)


    # Using cv2.line() method
    img = cv.line(img, leg1_start_point, leg1_end_point, silver_color, line_thickness)
    img = cv.line(img, trig_start_point, trig_end_point, silver_color, line_thickness)
    img = cv.line(img, echo_start_point, echo_end_point, silver_color, line_thickness)
    img = cv.line(img, gnd_start_point, gnd_end_point, silver_color, line_thickness)
    
    return img



def led_image(img):
    
    # Coordinates and angles
    startAngle = 0
    endAngle = 360
    center_coordinates = (250,45)
    axesLength = (20, 10)
    angle = 90
        
    # Leg 1
    leg1_start_point = (249, 105)
    leg1_end_point = (249, 52)
        
    # Leg 2
    leg2_start_point = (252, 120)
    leg2_end_point = (252, 52)
        
    # Text
    led_org = (242,15)

    # Using cv.line() method
    img = cv.line(img, leg1_start_point, leg1_end_point, silver_color, thickness_text)
    img = cv.line(img, leg2_start_point, leg2_end_point, silver_color, thickness_text)
        
    # Text
    img = cv.putText(img, 'Led', led_org, font, fontScale_3, black_color, thickness_text, cv.LINE_AA)
      
    # Draw a ellipse as a led
    img = cv.ellipse(img, center_coordinates, axesLength,angle, startAngle, endAngle, red_color, thickness)
    
    return img