import torch
import torchvision
from utils_model import preprocess
import torch.nn.functional as F
import torchvision.transforms as transforms
from draw import *

# Get model
dataset_len_categories = 4
DEVICE = torch.device("cuda" if torch.cuda.is_available() else "cpu")
model = torchvision.models.resnet18()
model.fc = torch.nn.Linear(512, dataset_len_categories)
model.load_state_dict(torch.load('model2.pth'))
model = model.to(DEVICE)
model.eval()

# Roi for prediction rectangles
left, top, right, bottom = 20, 100, 400, 450
left2, top2, right2, bottom2 = 500, 100, 880, 450

def return_predictions(frame):
    prediction = False
    # Get frames for prediction from rectangles left and right side
    roi_left = frame[top:bottom, left:right]
    roi_right = frame[top2:bottom2, left2:right2]

    # Preprocess frames
    preprocessed_left = preprocess(roi_left)
    preprocessed_right = preprocess(roi_right)

    # Detection model for devices
    with torch.no_grad():
        output_left = model(preprocessed_left)
        output_right = model(preprocessed_right)
        output_left = F.softmax(output_left, dim=1).detach().cpu().numpy().flatten()
        output_right = F.softmax(output_right, dim=1).detach().cpu().numpy().flatten()

        # Categories
        cat_list = ['raspberry', 'ultrasonic', 'led','button']

        # Output to list
        list_output_left = output_left.tolist()
        list_output_right = output_right.tolist()

        max_value_left = list_output_left.index(max(list_output_left))
        max_value_right = list_output_right.index(max(list_output_right))

        text_left = cat_list[max_value_left]
        text_right = cat_list[max_value_right]

        acc_left = max(list_output_left)
        acc_right = max(list_output_right)
        
        if(acc_left>0.7 and acc_right>0.7):
            # If raspberry and ultrasonic sensor
            if(text_left == cat_list[0] and text_right == cat_list[1]):
                save_to_image(frame, ultrasonic_image, ultrasonic_wires)
                prediction = True
      

            # If raspberry and ultrasonic sensor    
            elif(text_left == cat_list[1] and text_right == cat_list[0]):
                save_to_image(frame, ultrasonic_image, ultrasonic_wires)
                prediction = True


            # RPI and button
            elif(text_left == cat_list[0] and text_right == cat_list[3]):
                save_to_image(frame, button_image, button_wires)
                prediction = True
                           
   

            # RPI and button    
            elif(text_left == cat_list[3] and text_right == cat_list[0]):
                save_to_image(frame, button_image, button_wires)
                prediction = True
                            
            # LED 
            elif(acc_right >0.99 and text_left == cat_list[0] and text_right == cat_list[2]):
                save_to_image(frame, led_image, led_wires)
                prediction = True
                        
  

            # LED 
            elif(acc_left >0.99 and text_left == cat_list[2] and text_right == cat_list[0]):
                save_to_image(frame, led_image, led_wires)
                prediction = True

        
        return text_left, text_right, prediction